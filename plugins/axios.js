export default function ({ app }, inject) {
  // eslint-disable-next-line no-unused-vars
  const { $auth, $axios, $cookies } = app
  const authStrategy = $cookies.get('auth.strategy')

  $axios.onRequest((config) => {
    // console.log('Base URL', config.baseURL, config.method, config.url)

    $axios.setHeader('Content-Type', 'application/json')

    // const authToken = $cookies?.get(`auth._token.local`)
    const strategyToken = $cookies?.get(`auth._token.${authStrategy}`)
    // console.log('authToken', authToken)
    $axios.setHeader('Authorization', strategyToken)
  })

  $axios.onResponseError((error) => {
    console.log('onResponseError', error?.response?.data)
    console.log('onResponseError', error)
    try {
      if (error?.response?.status === 401) {
        // $cookies.removeAll()
        // window.location.reload()
      }
    } catch (err) {
      console.error('[Axios] onResponseError', err)
    }
  })
}
