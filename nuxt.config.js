export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  server: {
    port: 8080, // default: 3005
    host: '0.0.0.0', // default: localhost
  },
  ssr: false,
  head: {
    title: 'Test',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  loading: {
    color: '#722ED1',
    height: '4px',
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/style/style.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: ['~/plugins/axios.js'],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules

  buildModules: [
    // https://go.nuxtjs.dev/eslint
    [
      '@nuxtjs/eslint-module',
      {
        threads: true,
      },
    ],
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    'cookie-universal-nuxt',
    '@nuxtjs/auth-next',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    [
      'nuxt-i18n',
      {
        locales: [
          {
            code: 'ru',
            file: 'ru.js',
          },
          {
            code: 'uz',
            file: 'uz.js',
          },
          {
            code: 'oz',
            file: 'oz.js',
          },
        ],
        strategy: 'prefix_and_default',
        defaultLocale: 'oz',
        detectBrowserLanguage: false,
        lazy: true,
        vueI18nLoader: true,
        langDir: 'languages/',
      },
    ],
  ],
  auth: {
    // plugins: ["~/plugins/auth/index.js"],
    redirect: {
      login: '/callback',
      logout: '/login',
      // callback: "/callback",
      home: '/',
    },
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: 'token',
          global: false,
        },
        user: {
          property: false,
          autoFetch: true,
        },
        refreshToken: {
          property: 'refresh_token',
          data: 'refresh_token',
          maxAge: 60 * 60 * 24 * 30,
        },
        endpoints: {
          login: { url: '/management/login', method: 'post' },
          logout: false,
        },
      },
    },
    localStorage: false,
  },

  env: {
    VUE_APP_BASE: process.env.VUE_APP_BASE,
  },
  publicRuntimeConfig: {
    axios: {
      retry: { retries: 0 },
      baseURL: process.env.VUE_APP_BASE,
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL:
      process.env.VUE_APP_BASE || 'https://jsonplaceholder.typicode.com/',
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
